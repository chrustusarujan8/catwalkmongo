package com.example.catwalkmongo;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText email;
    EditText password;
    Button login, createaccount;
    TextView info;
    int counter = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email = findViewById(R.id.emailLogin);
        password=findViewById(R.id.passwordLogin);

        login = findViewById(R.id.btnlogin);
        createaccount = findViewById(R.id.btncreate);
        info = findViewById(R.id.info);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                   if (!validateEmail() | !validatePassword()) {
                       return;
                   }

                       register();


                   Intent i = new Intent(MainActivity.this, Profile.class);
                   startActivity(i);

            }
        });

        createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CreateAccount.class);
                startActivity(i);

            }
        });


    }

   /* boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }
*/

    boolean validatePassword(String password) {

        if (password.isEmpty()) {
            return true;
        } else if (password.length() < 8) {
            return true;
        } else {
            return false;
        }

    }

 /*   boolean CheckDataEntered() {

        if (isEmail(email) == false) {
            email.setError("Enter Valid Email");
            email.requestFocus();
            return false;
        }
        else if (validatePassword(password.getText().toString())) {
            password.setError("Enter valid password");
            password.setError("Password length is not enough");
            password.requestFocus();
            return false;
        } else {
            // Toast.makeText(Login.this,"Input validation Success",Toast.LENGTH_LONG).show();

            counter--;

            info.setText("No of attempts remaining" + String.valueOf(counter));
            if (counter == 0) {
                login.setEnabled(false);
                return true;
            }
            return true;
        }
    }*/
    private boolean validateEmail() {
        String emailInput=email.getText().toString().trim();
        if(emailInput.isEmpty()){
            email.setError("Fiend can,t be empty");
            return false;
        }
        else{
            email.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput=password.getText().toString().trim();
        if(passwordInput.isEmpty()){
            password.setError("Fiend can,t be empty");
            return false;
        }else if(passwordInput.length()<8){
            password.setError("Password is not strong");
            return false;
        }

        else{
            password.setError(null);
            return true;
        }
    }
    public void register()
    {
        final String uname = email.getText().toString().trim();
        final String pass = password.getText().toString().trim();



        String api = "http://192.168.8.102:8096/api/auth/signin";

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, api, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Rest Response", response.toString());
                Toast.makeText( MainActivity.this, "Rest Response\", response.toString", Toast.LENGTH_SHORT).show();

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Response", error.toString());
                        Toast.makeText(MainActivity.this, "Rest Response\", response.toString", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> data = new HashMap<String, String>();
                data.put("username", uname);
                data.put("password",pass);
                return data;
            }


        };
        RequestQueue requestQueue = (RequestQueue) Volley.newRequestQueue(this);
        requestQueue.add(objectRequest);
    }
}
