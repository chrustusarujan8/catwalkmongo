package com.example.catwalkmongo;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateAccount extends AppCompatActivity {

    TextInputLayout name,username,email,password;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        name = findViewById(R.id.firstName);
        username = findViewById(R.id.lastName);
        email = findViewById(R.id.emailSubmit);
        password = findViewById(R.id.passwordSubmit);
        submit = findViewById(R.id.btnsubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               register();

            }
        });

    }
    public void register()
    {
        final String uname = username.getEditText().getText().toString().trim();
        final String nam = name.getEditText().getText().toString().trim();
        final String pass = password.getEditText().getText().toString().trim();
        final String emailtext = email.getEditText().getText().toString().trim();


        String api = "http://192.168.8.102:8096/api/auth/signup";

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, api, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Rest Response", response.toString());
                Toast.makeText(CreateAccount.this, "Rest Response\", response.toString", Toast.LENGTH_SHORT).show();

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Response", error.toString());
                        Toast.makeText(CreateAccount.this, "Rest Response\", response.toString", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> data = new HashMap<String, String>();
                data.put("username", uname);
                data.put("name",nam);
                data.put("email",emailtext);
                //data.put("role","user");
                data.put("password",pass);



                return data;
            }


        };
        RequestQueue requestQueue = (RequestQueue) Volley.newRequestQueue(this);
        requestQueue.add(objectRequest);
    }
}
